import React from 'react';
// import logo from '../../logo.svg';
import styles from './App.module.css';
import './App.module.css';
import Item from '../../components/Item';
import listItem from '../../data';


// export default function App() {
//   const [txt, setTxt] = React.useState("defaultTxt");
//   const handleChange = event => {
//     const { value } = event.target;
//     setTxt(value);
//   };
//   return (
//     <div className="App">
//       <h1>Hello CodeSandbox</h1>
//       <h2>{txt}</h2>
//       <input onChange={handleChange} />
//     </div>
//   );
// }
//  spread operator
function App() {
  const content = listItem.map((item) =>
    <Item 
      order={item.order}
      pos={item.pos} 
      real={item.real} 
      link={item.link}
      title={item.title}
      author={item.author} 
      timere={item.timere}
    />
  );
  return (
    <div className="App">
      {content}
    </div>
  );
}

export default App;
