import React from 'react';
import styles from './style.module.css';
import './style.module.css';
import $ from 'jquery';

const Item = function(props){
  const [isopen,setOpen] = React.useState(false);
  const handleChange = event => {
    setOpen(!isopen);
  };
  return (
    <div>
      <div className={`${styles.item} ${styles.items}`}>
        <div>{props.order}</div>
        <div className={styles.position}>
          <span className={props.real}></span>
          <span>{props.pos}</span>
          </div>
        <div className={styles.imgbox}>
          <div className={styles.img}>
            <img src={props.link} alt=''></img>
          </div>
        </div>
        <div className={styles.cnt}>
          <a>{props.title}</a>
          <span>{props.author}</span>
        </div>
        <div className={styles.timere}>{props.timere}</div>
        <div className={styles.extends}>
          <div className='icon-play icon'></div>
          <div className='icon icon-add'></div>
          <div className='other-list'>
            <span className='icon-other icon' 
              onClick={event => handleChange(event)} 
              onChange={event => handleChange(event)}>
            </span>
            <div className='other'>
              {isopen && <div className='list'>
                {/* thay thế if bằng && và {} */}
                <ul>
                  <li>
                    <a href='#'>Thêm vào danh sách phát</a>
                  </li>
                  <li>
                    <a href='#'>Phát tiếp theo</a>
                  </li>
                  <li>
                    <a href='#'>Thêm vào playlist</a>
                  </li>
                  <li>
                    <a href='#'>Bình luận</a>
                  </li>
                  <li>
                    <a href='#'>Đóng góp lời bài hát</a>
                  </li>
                </ul>
              </div>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Item;